package net.strainovic.tagged.taggedrest.dao;

import com.mongodb.MongoClient;
import cz.jirutka.spring.embedmongo.EmbeddedMongoFactoryBean;
import net.strainovic.tagged.taggedrest.dao.impl.TaggedSearchDaoImpl;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@DataMongoTest
@RunWith(SpringRunner.class)
public abstract class AbstractDaoTest {

    @TestConfiguration
    static class TaggedSearchDaoTestConfiguration {
        private static final String MONGO_DB_URL = "localhost";
        private static final String MONGO_DB_NAME = "embeded_db";

        @Bean
        public TaggedSearchDao getTaggedSearchDao() {
            return new TaggedSearchDaoImpl();
        }

        @Bean
        public MongoTemplate mongoTemplate() throws IOException {
            EmbeddedMongoFactoryBean mongo = new EmbeddedMongoFactoryBean();
            mongo.setBindIp(MONGO_DB_URL);
            MongoClient mongoClient = mongo.getObject();
            return new MongoTemplate(mongoClient, MONGO_DB_NAME);
        }
    }
}
