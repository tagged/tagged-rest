package net.strainovic.tagged.taggedrest.dao;

import net.strainovic.tagged.taggedrest.model.Tagged;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;


public class TaggedDaoTest extends AbstractDaoTest {

    @Autowired
    private TaggedDao taggedDao;

    @Test
    public void CRUDTest() {
        List<Tagged> taggedList = taggedDao.findAll();
        Assert.assertEquals(0, taggedList.size());

        //Create
        String id = UUID.randomUUID().toString();
        Tagged tagged = new Tagged();
        tagged.setId(id);
        tagged.setName("Some name.doc");
        tagged.setTags(Arrays.asList("beach", "party"));
        tagged.setProvider("google drive");
        tagged.setProviderId("someRandomProviderId");

        taggedDao.save(tagged);

        tagged = taggedDao.findOne(id);
        Assert.assertNotNull(tagged);
        Assert.assertEquals("Some name.doc", tagged.getName());
        Assert.assertEquals(Arrays.asList("beach", "party"), tagged.getTags());
        Assert.assertEquals("google drive", tagged.getProvider());
        Assert.assertEquals("someRandomProviderId", tagged.getProviderId());

        //Update
        tagged.addTags(Collections.singletonList("sunset"));
        taggedDao.save(tagged);

        tagged = taggedDao.findOne(id);
        Assert.assertNotNull(tagged);
        Assert.assertEquals("Some name.doc", tagged.getName());
        Assert.assertEquals(Arrays.asList("beach", "party", "sunset"), tagged.getTags());

        //Deleted
        taggedDao.deleteById(id);
        taggedList = taggedDao.findAll();
        Assert.assertEquals(0, taggedList.size());

    }

    @After
    public void tearDown() {
        taggedDao.deleteAll();
    }

    @Test
    public void updateTagsTest() {
        //Create
        String id = UUID.randomUUID().toString();
        Tagged tagged = new Tagged();
        tagged.setId(id);
        tagged.setName("Some name.doc");
        tagged.setTags(Arrays.asList("beach", "party"));
        tagged.setProvider("google drive");

        taggedDao.save(tagged);

        Tagged taggedFromDb = taggedDao.findOne(id);
        taggedFromDb.setTags(Collections.singletonList("sunset"));

        taggedDao.save(taggedFromDb);

        taggedFromDb = taggedDao.findOne(id);
        Assert.assertEquals(1, taggedFromDb.getTags().size());
        Assert.assertEquals("sunset", taggedFromDb.getTags().get(0));
    }
}