package net.strainovic.tagged.taggedrest.dao;

import net.strainovic.tagged.taggedrest.controller.command.PageCommand;
import net.strainovic.tagged.taggedrest.controller.command.SearchCommand;
import net.strainovic.tagged.taggedrest.model.Tagged;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class TaggedSearchDaoTest extends AbstractDaoTest {

    @Autowired
    private TaggedDao taggedDao;

    @Autowired
    private TaggedSearchDao taggedSearchDao;

    @Test
    public void pageAllWithBarTagTest() {
        //Prepare data for search
        Tagged tagged1 = new Tagged(UUID.randomUUID().toString(), "BeachAndBar", Arrays.asList("beach", "bar"), "mstrainovic", null, null);
        Tagged tagged2 = new Tagged(UUID.randomUUID().toString(), "Beach", Collections.singletonList("beach"), "mstrainovic", null, null);
        Tagged tagged3 = new Tagged(UUID.randomUUID().toString(), "Bar", Collections.singletonList("bar"), "mstrainovic", null, null);
        Tagged tagged4 = new Tagged(UUID.randomUUID().toString(), "Ferrari", Collections.singletonList("f1"), "mstrainovic", null, null);
        taggedDao.save(Arrays.asList(tagged1, tagged2, tagged3, tagged4));

        PageRequest pageRequest = new PageRequest(0, 10);

        PageCommand pageCommand = new PageCommand();
        pageCommand.setTags(Collections.singletonList("bar"));
        pageCommand.setPageRequest(pageRequest);

        Page<Tagged> taggedPage = taggedSearchDao.page(pageCommand);
        Assert.assertEquals(2, taggedPage.getTotalElements());

        pageCommand.setTags(Arrays.asList("bar", "f1"));
        taggedPage = taggedSearchDao.page(pageCommand);
        Assert.assertEquals(3, taggedPage.getTotalElements());

        pageCommand.setTags(Arrays.asList("bar", "beach"));
        taggedPage = taggedSearchDao.page(pageCommand);
        Assert.assertEquals(3, taggedPage.getTotalElements());

        pageCommand.setTags(Collections.singletonList("sunset"));
        taggedPage = taggedSearchDao.page(pageCommand);
        Assert.assertEquals(0, taggedPage.getTotalElements());
    }

    @Test
    public void pageByTagsAndProviderTest() {
        //Prepare data for search
        Tagged tagged1 = new Tagged(UUID.randomUUID().toString(), "BeachAndBar", Arrays.asList("beach", "bar"), "mstrainovic", "google drive", null);
        Tagged tagged2 = new Tagged(UUID.randomUUID().toString(), "Beach", Collections.singletonList("beach"), "mstrainovic", null, null);
        Tagged tagged3 = new Tagged(UUID.randomUUID().toString(), "Bar", Collections.singletonList("bar"), "mstrainovic", "dropbox", null);
        Tagged tagged4 = new Tagged(UUID.randomUUID().toString(), "Ferrari", Collections.singletonList("f1"), "mstrainovic", null, null);
        taggedDao.save(Arrays.asList(tagged1, tagged2, tagged3, tagged4));

        PageRequest pageRequest = new PageRequest(0, 10);

        //Search without provider
        PageCommand pageCommand = new PageCommand();
        pageCommand.setTags(Collections.singletonList("bar"));
        pageCommand.setPageRequest(pageRequest);


        //Paged search
        Page<Tagged> taggedPage = taggedSearchDao.page(pageCommand);
        Assert.assertEquals(2, taggedPage.getTotalElements());

        //Search with provider
        pageCommand.setProvider("dropbox");
        taggedPage = taggedSearchDao.page(pageCommand);
        Assert.assertEquals(1, taggedPage.getTotalElements());

        //Search with unknown provider
        pageCommand.setProvider("drop");
        taggedPage = taggedSearchDao.page(pageCommand);
        Assert.assertEquals(0, taggedPage.getTotalElements());
    }

    @Test
    public void pageByTagsMultiplePagesSortTest() {
        //Prepare data for page
        Tagged tagged1 = new Tagged(UUID.randomUUID().toString(), "page1", Collections.singletonList("page"), "mstrainovic", null, null);
        Tagged tagged2 = new Tagged(UUID.randomUUID().toString(), "page2", Collections.singletonList("page"), "mstrainovic", null, null);
        Tagged tagged3 = new Tagged(UUID.randomUUID().toString(), "page3", Collections.singletonList("page"), "mstrainovic", null, null);
        Tagged tagged4 = new Tagged(UUID.randomUUID().toString(), "page4", Collections.singletonList("page"), "mstrainovic", null, null);
        Tagged tagged5 = new Tagged(UUID.randomUUID().toString(), "page5", Collections.singletonList("page"), "mstrainovic", null, null);
        Tagged tagged6 = new Tagged(UUID.randomUUID().toString(), "page6", Collections.singletonList("page"), "mstrainovic", null, null);
        taggedDao.save(Arrays.asList(tagged1, tagged2, tagged3, tagged4, tagged5, tagged6));

        //Find first page ordered by name DESC
        Sort.Order order = new Sort.Order(Sort.Direction.DESC, "name");
        Sort sort = new Sort(order);
        PageRequest pageRequest = new PageRequest(0, 4, sort);

        PageCommand pageCommand = new PageCommand();
        pageCommand.setTags(Collections.singletonList("page"));
        pageCommand.setPageRequest(pageRequest);

        Page<Tagged> taggedPage = taggedSearchDao.page(pageCommand);

        //Validate total count
        Assert.assertEquals(6, taggedPage.getTotalElements());
        Assert.assertEquals(2, taggedPage.getTotalPages());

        //Validate first page
        Assert.assertEquals(4, taggedPage.getNumberOfElements());
        Assert.assertEquals("page6", taggedPage.getContent().get(0).getName());
        Assert.assertEquals("page5", taggedPage.getContent().get(1).getName());
        Assert.assertEquals("page4", taggedPage.getContent().get(2).getName());
        Assert.assertEquals("page3", taggedPage.getContent().get(3).getName());

        //Validate second page
        pageCommand.setPageRequest((PageRequest) pageCommand.getPageRequest().next());
        taggedPage = taggedSearchDao.page(pageCommand);
        Assert.assertEquals(2, taggedPage.getNumberOfElements());
        Assert.assertEquals("page2", taggedPage.getContent().get(0).getName());
        Assert.assertEquals("page1", taggedPage.getContent().get(1).getName());
    }

    @Test
    public void searchAllWithBarTagTest() {
        //Prepare data for search
        Tagged tagged1 = new Tagged(UUID.randomUUID().toString(), "BeachAndBar", Arrays.asList("beach", "bar"), "mstrainovic", null, null);
        Tagged tagged2 = new Tagged(UUID.randomUUID().toString(), "Beach", Collections.singletonList("beach"), "mstrainovic", null, null);
        Tagged tagged3 = new Tagged(UUID.randomUUID().toString(), "Bar", Collections.singletonList("bar"), "mstrainovic", null, null);
        Tagged tagged4 = new Tagged(UUID.randomUUID().toString(), "Ferrari", Collections.singletonList("f1"), "mstrainovic", null, null);
        taggedDao.save(Arrays.asList(tagged1, tagged2, tagged3, tagged4));

        SearchCommand searchCommand = new SearchCommand();
        searchCommand.setTags(Collections.singletonList("bar"));

        List<Tagged> search = taggedSearchDao.search(searchCommand);
        Assert.assertEquals(2, search.size());

        searchCommand.setTags(Arrays.asList("bar", "f1"));
        search = taggedSearchDao.search(searchCommand);
        Assert.assertEquals(3, search.size());

        searchCommand.setTags(Arrays.asList("bar", "beach"));
        search = taggedSearchDao.search(searchCommand);
        Assert.assertEquals(3, search.size());

        searchCommand.setTags(Collections.singletonList("sunset"));
        search = taggedSearchDao.search(searchCommand);
        Assert.assertEquals(0, search.size());
    }

    @Test
    public void searchByTagsAndProviderTest() {
        //Prepare data for search
        Tagged tagged1 = new Tagged(UUID.randomUUID().toString(), "BeachAndBar", Arrays.asList("beach", "bar"), "mstrainovic", "google drive", null);
        Tagged tagged2 = new Tagged(UUID.randomUUID().toString(), "Beach", Collections.singletonList("beach"), "mstrainovic", null, null);
        Tagged tagged3 = new Tagged(UUID.randomUUID().toString(), "Bar", Collections.singletonList("bar"), "mstrainovic", "dropbox", null);
        Tagged tagged4 = new Tagged(UUID.randomUUID().toString(), "Ferrari", Collections.singletonList("f1"), "mstrainovic", null, null);
        taggedDao.save(Arrays.asList(tagged1, tagged2, tagged3, tagged4));

        //Search without provider
        SearchCommand searchCommand = new SearchCommand();
        searchCommand.setTags(Collections.singletonList("bar"));

        List<Tagged> search = taggedSearchDao.search(searchCommand);
        Assert.assertEquals(2, search.size());

        //Search with provider
        searchCommand.setProvider("dropbox");
        search = taggedSearchDao.search(searchCommand);
        Assert.assertEquals(1, search.size());

        //Search with unknown provider
        searchCommand.setProvider("drop");
        search = taggedSearchDao.search(searchCommand);
        Assert.assertEquals(0, search.size());
    }

    @Test
    public void searchByExternalIdAndProviderTest() {
        String externalId1 = UUID.randomUUID().toString();
        String externalId2 = UUID.randomUUID().toString();
        String externalId3 = UUID.randomUUID().toString();
        String externalId4 = UUID.randomUUID().toString();

        //Prepare data for search
        Tagged tagged1 = new Tagged(externalId1, "BeachAndBar", Arrays.asList("beach", "bar"), "mstrainovic", "google drive", null);
        Tagged tagged2 = new Tagged(externalId2, "Beach", Collections.singletonList("beach"), "mstrainovic", "DROPBOX", null);
        Tagged tagged3 = new Tagged(externalId3, "Bar", Collections.singletonList("bar"), "mstrainovic", "DROPBOX", "SomeOtherProviderIs");
        Tagged tagged4 = new Tagged(externalId4, "Ferrari", Collections.singletonList("f1"), "mstrainovic", "DROPBOX", "someProviderId");
        taggedDao.save(Arrays.asList(tagged1, tagged2, tagged3, tagged4));

        //Search without provider
        SearchCommand searchCommand = new SearchCommand();
        searchCommand.setProvider("DROPBOX");
        searchCommand.setFileIds(Collections.singletonList(externalId1));

        //Search with wrong provider
        List<Tagged> search = taggedSearchDao.search(searchCommand);
        Assert.assertEquals(0, search.size());

        //Search with provider
        searchCommand.setFileIds(Collections.singletonList(externalId2));
        search = taggedSearchDao.search(searchCommand);
        Assert.assertEquals(1, search.size());

        //Search multiple ids
        searchCommand.setFileIds(Arrays.asList(externalId1, externalId2, externalId3, externalId4));
        search = taggedSearchDao.search(searchCommand);
        Assert.assertEquals(3, search.size());

        //Search by provider id
        searchCommand = new SearchCommand();
        searchCommand.setProviderId("someProviderId");
        search = taggedSearchDao.search(searchCommand);
        Assert.assertEquals(1, search.size());
        Assert.assertEquals("Ferrari", search.get(0).getName());

    }


    @After
    public void tearDown() {
        taggedDao.deleteAll();
    }

}