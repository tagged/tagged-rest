package net.strainovic.tagged.taggedrest.controller.command;

import org.junit.Assert;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

public class AddTagCommandTest extends AbstractCommandTest {

    @Test
    public void usernameIsBlankNullTest() {
        AddTagCommand addTagCommand = new AddTagCommand();
        addTagCommand.setFileId("someTestId");
        addTagCommand.setName("File name.docx");
        addTagCommand.setTags(Arrays.asList("beach", "party"));
        addTagCommand.setUsername("");

        Set<ConstraintViolation<Object>> result = validate(addTagCommand);
        Iterator<ConstraintViolation<Object>> iterator = result.iterator();
        ConstraintViolation<Object> cv = iterator.next();

        Assert.assertEquals("may not be empty", cv.getMessage());
        Assert.assertEquals("username", cv.getPropertyPath().toString());
        Assert.assertEquals("", cv.getInvalidValue());
        Assert.assertFalse(iterator.hasNext());
    }

    @Test
    public void usernameIsNullTest() {
        AddTagCommand addTagCommand = new AddTagCommand();
        addTagCommand.setFileId("someTestId");
        addTagCommand.setName("File name.docx");
        addTagCommand.setTags(Arrays.asList("beach", "party"));

        Set<ConstraintViolation<Object>> result = validate(addTagCommand);
        Iterator<ConstraintViolation<Object>> iterator = result.iterator();
        ConstraintViolation<Object> cv = iterator.next();

        Assert.assertEquals("may not be empty", cv.getMessage());
        Assert.assertEquals("username", cv.getPropertyPath().toString());
        Assert.assertNull(cv.getInvalidValue());
        Assert.assertFalse(iterator.hasNext());
    }

    @Test
    public void validateIdIsNullTest() {
        AddTagCommand addTagCommand = new AddTagCommand();
        addTagCommand.setName("File name.docx");
        addTagCommand.setTags(Arrays.asList("beach", "party"));
        addTagCommand.setUsername("mstrainovic");

        Set<ConstraintViolation<Object>> result = validate(addTagCommand);
        Iterator<ConstraintViolation<Object>> iterator = result.iterator();
        ConstraintViolation<Object> cv = iterator.next();

        Assert.assertEquals("may not be empty", cv.getMessage());
        Assert.assertEquals("fileId", cv.getPropertyPath().toString());
        Assert.assertNull(cv.getInvalidValue());
        Assert.assertFalse(iterator.hasNext());
    }

    @Test
    public void validateInvalidTagsEmptyTest() {
        AddTagCommand addTagCommand = new AddTagCommand();
        addTagCommand.setFileId("someId");
        addTagCommand.setName("File name.docx");
        addTagCommand.setTags(Collections.singletonList("b@r"));
        addTagCommand.setUsername("mstrainovic");

        Set<ConstraintViolation<Object>> result = validate(addTagCommand);
        Iterator<ConstraintViolation<Object>> iterator = result.iterator();
        ConstraintViolation<Object> cv = iterator.next();

        Assert.assertEquals("For tag only leather or digit are allowed", cv.getMessage());
        Assert.assertEquals("tags", cv.getPropertyPath().toString());
        Assert.assertEquals(Collections.singletonList("b@r"), cv.getInvalidValue());
        Assert.assertFalse(iterator.hasNext());
    }

    @Test
    public void validateNameIsNullTest() {
        AddTagCommand addTagCommand = new AddTagCommand();
        addTagCommand.setFileId("someId");
        addTagCommand.setName("");
        addTagCommand.setTags(Arrays.asList("beach", "party"));
        addTagCommand.setUsername("mstrainovic");

        Set<ConstraintViolation<Object>> result = validate(addTagCommand);
        Iterator<ConstraintViolation<Object>> iterator = result.iterator();
        ConstraintViolation<Object> cv = iterator.next();

        Assert.assertEquals("may not be empty", cv.getMessage());
        Assert.assertEquals("name", cv.getPropertyPath().toString());
        Assert.assertEquals("", cv.getInvalidValue());
        Assert.assertFalse(iterator.hasNext());
    }

    @Test
    public void validateTagsBlankEntryTest() {
        AddTagCommand addTagCommand = new AddTagCommand();
        addTagCommand.setFileId("someId");
        addTagCommand.setName("File name.docx");
        addTagCommand.setTags(Collections.singletonList(""));
        addTagCommand.setUsername("mstrainovic");

        Set<ConstraintViolation<Object>> result = validate(addTagCommand);
        Iterator<ConstraintViolation<Object>> iterator = result.iterator();
        ConstraintViolation<Object> cv = iterator.next();

        Assert.assertEquals("For tag only leather or digit are allowed", cv.getMessage());
        Assert.assertEquals("tags", cv.getPropertyPath().toString());
        Assert.assertEquals(Collections.singletonList(""), cv.getInvalidValue());
        Assert.assertFalse(iterator.hasNext());
    }

    @Test
    public void validateTagsEmptyTest() {
        AddTagCommand addTagCommand = new AddTagCommand();
        addTagCommand.setFileId("someId");
        addTagCommand.setName("File name.docx");
        addTagCommand.setTags(Collections.emptyList());
        addTagCommand.setUsername("mstrainovic");

        Set<ConstraintViolation<Object>> result = validate(addTagCommand);
        Iterator<ConstraintViolation<Object>> iterator = result.iterator();
        ConstraintViolation<Object> cv = iterator.next();

        Assert.assertEquals("For tag only leather or digit are allowed", cv.getMessage());
        Assert.assertEquals("tags", cv.getPropertyPath().toString());
        Assert.assertEquals(Collections.emptyList(), cv.getInvalidValue());
        Assert.assertFalse(iterator.hasNext());
    }

    @Test
    public void validateTagsInvalidCharTest() {
        AddTagCommand addTagCommand = new AddTagCommand();
        addTagCommand.setFileId("someId");
        addTagCommand.setName("File name.docx");
        addTagCommand.setTags(Arrays.asList("beach", "part^y"));
        addTagCommand.setUsername("mstrainovic");

        Set<ConstraintViolation<Object>> result = validate(addTagCommand);
        Iterator<ConstraintViolation<Object>> iterator = result.iterator();
        ConstraintViolation<Object> cv = iterator.next();

        Assert.assertEquals("For tag only leather or digit are allowed", cv.getMessage());
        Assert.assertEquals("tags", cv.getPropertyPath().toString());
        Assert.assertEquals(Arrays.asList("beach", "part^y"), cv.getInvalidValue());
        Assert.assertFalse(iterator.hasNext());
    }

    @Test
    public void validateTagsIsNullTest() {
        AddTagCommand addTagCommand = new AddTagCommand();
        addTagCommand.setFileId("someId");
        addTagCommand.setName("File name.docx");
        addTagCommand.setUsername("mstrainovic");

        Set<ConstraintViolation<Object>> result = validate(addTagCommand);
        Iterator<ConstraintViolation<Object>> iterator = result.iterator();
        ConstraintViolation<Object> cv = iterator.next();

        Assert.assertEquals("For tag only leather or digit are allowed", cv.getMessage());
        Assert.assertEquals("tags", cv.getPropertyPath().toString());
        Assert.assertNull(cv.getInvalidValue());
        Assert.assertFalse(iterator.hasNext());
    }

}