package net.strainovic.tagged.taggedrest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.strainovic.tagged.taggedrest.controller.command.PageCommand;
import net.strainovic.tagged.taggedrest.controller.command.SearchCommand;
import net.strainovic.tagged.taggedrest.controller.command.dto.PageCommandDto;
import net.strainovic.tagged.taggedrest.controller.command.dto.SearchCommandDto;
import net.strainovic.tagged.taggedrest.dao.TaggedSearchDao;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SearchController.class)
public class SearchControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TaggedSearchDao taggedSearchDao;

    @Test
    public void pagePositiveTest() throws Exception {
        PageCommandDto dto = new PageCommandDto();
        dto.setTags(Collections.singletonList("beach"));
        dto.setProvider("dropbox");

        mvc.perform(post("/v1/tags/page")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(dto))).andExpect(status().isOk());

        ArgumentCaptor<PageCommand> captor = ArgumentCaptor.forClass(PageCommand.class);
        verify(taggedSearchDao, times(1)).page(captor.capture());

        PageCommand pageCommand = captor.getValue();
        Assert.assertEquals(1, pageCommand.getTags().size());
        Assert.assertEquals("beach", pageCommand.getTags().iterator().next());
        Assert.assertEquals("dropbox", pageCommand.getProvider());

        Sort.Order order = new Sort.Order(Sort.Direction.ASC, "name");
        Sort sort = new Sort(order);

        Assert.assertEquals(0, pageCommand.getPageRequest().getPageNumber());
        Assert.assertEquals(25, pageCommand.getPageRequest().getPageSize());
        Assert.assertEquals(sort, pageCommand.getPageRequest().getSort());
    }

    @Test
    public void searchPositiveTest() throws Exception {
        SearchCommandDto dto = new SearchCommandDto();
        dto.setTags(Collections.singletonList("beach"));
        dto.setProvider("dropbox");

        mvc.perform(post("/v1/tags/search")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(dto))).andExpect(status().isOk());

        ArgumentCaptor<SearchCommand> captor = ArgumentCaptor.forClass(SearchCommand.class);
        verify(taggedSearchDao, times(1)).search(captor.capture());

        SearchCommand searchCommand = captor.getValue();
        Assert.assertEquals(1, searchCommand.getTags().size());
        Assert.assertEquals("beach", searchCommand.getTags().iterator().next());
        Assert.assertEquals("dropbox", searchCommand.getProvider());
    }

}