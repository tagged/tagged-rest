package net.strainovic.tagged.taggedrest.controller.dto;

import org.junit.BeforeClass;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public abstract class AbstractDtoTest {

    private static Validator validator;

    @BeforeClass
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    private static Validator getValidator() {
        return validator;
    }

    Set<ConstraintViolation<Object>> validate(Object dtoClass) {
        return getValidator().validate(dtoClass);
    }

}