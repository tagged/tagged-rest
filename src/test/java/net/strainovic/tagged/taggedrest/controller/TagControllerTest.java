package net.strainovic.tagged.taggedrest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.strainovic.tagged.taggedrest.controller.command.AddTagCommand;
import net.strainovic.tagged.taggedrest.controller.command.EditTagCommand;
import net.strainovic.tagged.taggedrest.controller.command.SearchCommand;
import net.strainovic.tagged.taggedrest.dao.TaggedDao;
import net.strainovic.tagged.taggedrest.dao.TaggedSearchDao;
import net.strainovic.tagged.taggedrest.model.Tagged;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TagController.class)
public class TagControllerTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TaggedDao taggedDao;

    @MockBean
    private TaggedSearchDao taggedSearchDao;

    @Test
    public void addTagsInvalidFieldTest() throws Exception {
        AddTagCommand addTagCommand = new AddTagCommand();
        addTagCommand.setName("File name.docx");
        addTagCommand.setTags(Arrays.asList("beach", "party"));

        mvc.perform(post("/v1/tags")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(addTagCommand)))
                .andExpect(status().isBadRequest());

        verify(taggedDao, never()).save(any(Tagged.class));
    }

    @Test
    public void addTagsSimplePositiveScenarioTest() throws Exception {
        AddTagCommand addTagCommand = new AddTagCommand();
        addTagCommand.setFileId("someId");
        addTagCommand.setName("File name.docx");
        addTagCommand.setTags(Arrays.asList("beach", "party"));
        addTagCommand.setProvider("dropbox");
        addTagCommand.setUsername("mstrainovic");

        mvc.perform(post("/v1/tags")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(addTagCommand)))
                .andExpect(status().isOk());

        ArgumentCaptor<Tagged> captor = ArgumentCaptor.forClass(Tagged.class);
        verify(taggedDao, times(1)).save(captor.capture());

        Tagged tagged = captor.getValue();
        Assert.assertEquals("someId", tagged.getFileId());
        Assert.assertEquals("File name.docx", tagged.getName());
        Assert.assertEquals(Arrays.asList("beach", "party"), tagged.getTags());
        Assert.assertEquals("DROPBOX", tagged.getProvider());
        Assert.assertEquals("mstrainovic", tagged.getUsername());
    }

    @Test
    public void addTagsWithSerbianCharsTest() throws Exception {
        AddTagCommand addTagCommand = new AddTagCommand();
        addTagCommand.setFileId("someId");
        addTagCommand.setName("File name.docx");
        addTagCommand.setTags(Arrays.asList("plaža", "party"));
        addTagCommand.setUsername("mstrainovic");

        when(taggedSearchDao.search(any(SearchCommand.class))).thenReturn(Collections.emptyList());

        mvc.perform(post("/v1/tags")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(addTagCommand)))
                .andExpect(status().isOk());

        ArgumentCaptor<Tagged> captor = ArgumentCaptor.forClass(Tagged.class);
        verify(taggedDao, times(1)).save(captor.capture());

        Tagged tagged = captor.getValue();
        Assert.assertEquals("someId", tagged.getFileId());
        Assert.assertEquals("File name.docx", tagged.getName());
        Assert.assertEquals(Arrays.asList("plaža", "party"), tagged.getTags());
        Assert.assertEquals("mstrainovic", tagged.getUsername());

    }

    @Test
    public void deleteTags() throws Exception {
        String uuid = UUID.randomUUID().toString();

        mvc.perform(delete("/v1/tags/" + uuid)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(uuid)))
                .andExpect(status().isOk());

        verify(taggedDao, times(1)).deleteById(uuid);
    }

    @Test
    public void deleteTagsMissingRequestParamTest() throws Exception {
        mvc.perform(delete("/v1/tags/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed());

        verify(taggedDao, never()).deleteById(anyString());
    }

    @Test
    public void editTagTaggedDoesntExistsForIdTest() throws Exception {
        expectedException.expect(NestedServletException.class);
        expectedException.expectMessage(Matchers.containsString("Tag id: someOtherId doesn't exist"));

        String uuid = "someRandomId";

        when(taggedDao.findOne(uuid)).thenReturn(null);

        EditTagCommand editTagCommand = new EditTagCommand();
        editTagCommand.setId("someOtherId");
        editTagCommand.setTags(Arrays.asList("whisky", "bourbon"));

        mvc.perform(put("/v1/tags")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(editTagCommand)))
                .andExpect(status().isOk());

        verify(taggedDao, never()).save(any(Tagged.class));

    }

    @Test
    public void editTagInvalidRequestTest() throws Exception {
        String uuid = UUID.randomUUID().toString();

        EditTagCommand editTagCommand = new EditTagCommand();
        editTagCommand.setId(uuid);

        mvc.perform(put("/v1/tags")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(editTagCommand)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void editTagPositiveTest() throws Exception {
        String uuid = UUID.randomUUID().toString();

        Tagged tagged = new Tagged();
        tagged.setId(uuid);
        tagged.setUsername("mstrainovic");
        tagged.addTags(Collections.singletonList("bar"));

        when(taggedDao.findOne(uuid)).thenReturn(tagged);

        EditTagCommand editTagCommand = new EditTagCommand();
        editTagCommand.setId(uuid);
        editTagCommand.setTags(Arrays.asList("whisky", "bourbon"));

        mvc.perform(put("/v1/tags")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(editTagCommand)))
                .andExpect(status().isOk());

        ArgumentCaptor<Tagged> taggedCaptor = ArgumentCaptor.forClass(Tagged.class);
        verify(taggedDao, times(1)).save(taggedCaptor.capture());

        Tagged capturedValue = taggedCaptor.getValue();
        Assert.assertEquals(2, capturedValue.getTags().size());
        Assert.assertFalse(capturedValue.getTags().contains("bar"));
        Assert.assertTrue(capturedValue.getTags().contains("whisky"));
        Assert.assertTrue(capturedValue.getTags().contains("bourbon"));
    }

}