package net.strainovic.tagged.taggedrest.controller.command;

import org.junit.Assert;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

public class EditTagCommandTest extends AbstractCommandTest {


    @Test
    public void validateIdIsNullTest() {
        EditTagCommand editTagCommand = new EditTagCommand();
        editTagCommand.setTags(Arrays.asList("beach", "party"));

        Set<ConstraintViolation<Object>> result = validate(editTagCommand);
        Iterator<ConstraintViolation<Object>> iterator = result.iterator();
        ConstraintViolation<Object> cv = iterator.next();

        Assert.assertEquals("may not be empty", cv.getMessage());
        Assert.assertEquals("id", cv.getPropertyPath().toString());
        Assert.assertNull(cv.getInvalidValue());
        Assert.assertFalse(iterator.hasNext());
    }

    @Test
    public void validateInvalidTagsEmptyTest() {
        EditTagCommand editTagCommand = new EditTagCommand();
        editTagCommand.setId("someId");
        editTagCommand.setTags(Collections.singletonList("b@r"));

        Set<ConstraintViolation<Object>> result = validate(editTagCommand);
        Iterator<ConstraintViolation<Object>> iterator = result.iterator();
        ConstraintViolation<Object> cv = iterator.next();

        Assert.assertEquals("For tag only leather or digit are allowed", cv.getMessage());
        Assert.assertEquals("tags", cv.getPropertyPath().toString());
        Assert.assertEquals(Collections.singletonList("b@r"), cv.getInvalidValue());
        Assert.assertFalse(iterator.hasNext());
    }

    @Test
    public void validateTagsBlankEntryTest() {
        EditTagCommand editTagCommand = new EditTagCommand();
        editTagCommand.setId("someId");
        editTagCommand.setTags(Collections.singletonList(""));

        Set<ConstraintViolation<Object>> result = validate(editTagCommand);
        Iterator<ConstraintViolation<Object>> iterator = result.iterator();
        ConstraintViolation<Object> cv = iterator.next();

        Assert.assertEquals("For tag only leather or digit are allowed", cv.getMessage());
        Assert.assertEquals("tags", cv.getPropertyPath().toString());
        Assert.assertEquals(Collections.singletonList(""), cv.getInvalidValue());
        Assert.assertFalse(iterator.hasNext());
    }

    /**
     * Empty tags are NOT valid for edit
     */
    @Test
    public void validateTagsEmptyTest() {
        EditTagCommand editTagCommand = new EditTagCommand();
        editTagCommand.setId("someId");
        editTagCommand.setTags(Collections.emptyList());

        Set<ConstraintViolation<Object>> result = validate(editTagCommand);
        Iterator<ConstraintViolation<Object>> iterator = result.iterator();
        ConstraintViolation<Object> cv = iterator.next();

        Assert.assertEquals("For tag only leather or digit are allowed", cv.getMessage());
        Assert.assertEquals("tags", cv.getPropertyPath().toString());
        Assert.assertEquals(Collections.emptyList(), cv.getInvalidValue());
        Assert.assertFalse(iterator.hasNext());
    }

    @Test
    public void validateTagsInvalidCharTest() {
        EditTagCommand editTagCommand = new EditTagCommand();
        editTagCommand.setId("someId");
        editTagCommand.setTags(Arrays.asList("beach", "part^y"));

        Set<ConstraintViolation<Object>> result = validate(editTagCommand);
        Iterator<ConstraintViolation<Object>> iterator = result.iterator();
        ConstraintViolation<Object> cv = iterator.next();

        Assert.assertEquals("For tag only leather or digit are allowed", cv.getMessage());
        Assert.assertEquals("tags", cv.getPropertyPath().toString());
        Assert.assertEquals(Arrays.asList("beach", "part^y"), cv.getInvalidValue());
        Assert.assertFalse(iterator.hasNext());
    }

    /**
     * Null tags are NOT valid for edit
     */
    @Test
    public void validateTagsIsNullTest() {
        EditTagCommand editTagCommand = new EditTagCommand();
        editTagCommand.setId("someId");

        Set<ConstraintViolation<Object>> result = validate(editTagCommand);
        Iterator<ConstraintViolation<Object>> iterator = result.iterator();
        ConstraintViolation<Object> cv = iterator.next();

        Assert.assertEquals("For tag only leather or digit are allowed", cv.getMessage());
        Assert.assertEquals("tags", cv.getPropertyPath().toString());
        Assert.assertNull(cv.getInvalidValue());
        Assert.assertFalse(iterator.hasNext());
    }

}