package net.strainovic.tagged.taggedrest.controller.command.converter;

import net.strainovic.tagged.taggedrest.controller.command.SearchCommand;
import net.strainovic.tagged.taggedrest.controller.command.dto.SearchCommandDto;

public final class SearchCommandConverter {

    public static SearchCommand fromDtoToInternal(SearchCommandDto dto) {
        SearchCommand internal = new SearchCommand();
        internal.setFileIds(dto.getFileIds());
        internal.setTags(dto.getTags());
        internal.setProvider(dto.getProvider());
        internal.setUsername(dto.getUsername());
        return internal;
    }
}
