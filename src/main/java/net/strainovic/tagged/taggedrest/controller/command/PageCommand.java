package net.strainovic.tagged.taggedrest.controller.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.domain.PageRequest;

@ApiModel
public class PageCommand extends SearchCommand {

    @ApiModelProperty(required = true)
    private PageRequest pageRequest = new PageRequest(0, 25);

    public PageRequest getPageRequest() {
        return pageRequest;
    }

    public void setPageRequest(PageRequest pageRequest) {
        this.pageRequest = pageRequest;
    }

}
