package net.strainovic.tagged.taggedrest.controller.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.strainovic.tagged.taggedrest.model.validation.Tags;
import org.hibernate.validator.constraints.NotBlank;

import java.util.List;

@ApiModel
public class EditTagCommand {

    @NotBlank
    @ApiModelProperty(required = true)
    private String id;

    @Tags
    @ApiModelProperty(required = true)
    private List<String> tags;

    public String getId() {
        return id;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()
                + "[id: " + id
                + " tags: " + tags + "]";
    }
}
