package net.strainovic.tagged.taggedrest.controller.command.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;

@ApiModel
public class PageCommandDto extends SearchCommandDto {

    @Valid
    @ApiModelProperty
    private PageDto pageDto = new PageDto();

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}
