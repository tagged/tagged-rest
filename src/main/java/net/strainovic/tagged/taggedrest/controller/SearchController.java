package net.strainovic.tagged.taggedrest.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.strainovic.tagged.taggedrest.controller.command.PageCommand;
import net.strainovic.tagged.taggedrest.controller.command.SearchCommand;
import net.strainovic.tagged.taggedrest.controller.command.converter.PageCommandConverter;
import net.strainovic.tagged.taggedrest.controller.command.converter.SearchCommandConverter;
import net.strainovic.tagged.taggedrest.controller.command.dto.PageCommandDto;
import net.strainovic.tagged.taggedrest.controller.command.dto.SearchCommandDto;
import net.strainovic.tagged.taggedrest.dao.TaggedSearchDao;
import net.strainovic.tagged.taggedrest.model.Tagged;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Api(value = "Tags", consumes = "application/json", tags = "Tags")
@RestController
public class SearchController {

    @Autowired
    private TaggedSearchDao taggedSearchDao;

    @ApiOperation(value = "Search paged tags with page command", httpMethod = "POST")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return page of results"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @PostMapping("v1/tags/page")
    public Page<Tagged> page(@RequestBody @Valid PageCommandDto pageCommandDto) {
        PageCommand searchCommand = PageCommandConverter.fromDtoToInternal(pageCommandDto);

        return getTaggedSearchDao().page(searchCommand);
    }

    @ApiOperation(value = "Search all tags with search command", httpMethod = "POST")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return list of tag entries"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @PostMapping("v1/tags/search")
    public List<Tagged> search(@RequestBody @Valid SearchCommandDto searchCommandDto) {
        SearchCommand searchCommand = SearchCommandConverter.fromDtoToInternal(searchCommandDto);

        return getTaggedSearchDao().search(searchCommand);
    }

    private TaggedSearchDao getTaggedSearchDao() {
        return taggedSearchDao;
    }

}
