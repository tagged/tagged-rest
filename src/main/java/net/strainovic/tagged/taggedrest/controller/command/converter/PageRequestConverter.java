package net.strainovic.tagged.taggedrest.controller.command.converter;

import net.strainovic.tagged.taggedrest.controller.command.dto.PageDto;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

public final class PageRequestConverter {

    public static PageRequest fromDtoToInternal(PageDto pageDto) {
        Sort.Direction direction = "ASC".equals(pageDto.getDirection()) ? Sort.Direction.ASC : Sort.Direction.DESC;
        Sort.Order order = new Sort.Order(direction, pageDto.getSortProperty(), Sort.NullHandling.NATIVE);
        Sort sort = new Sort(order);
        return new PageRequest(pageDto.getPage(), pageDto.getSize(), sort);
    }
}
