package net.strainovic.tagged.taggedrest.controller.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.strainovic.tagged.taggedrest.model.validation.Tags;
import org.hibernate.validator.constraints.NotBlank;

import java.util.List;

@ApiModel
public class AddTagCommand {

    @NotBlank
    @ApiModelProperty(required = true)
    private String fileId;

    @NotBlank
    @ApiModelProperty(required = true)
    private String name;

    @ApiModelProperty
    private String provider;

    @ApiModelProperty
    private String providerId;

    @Tags
    @ApiModelProperty(required = true)
    private List<String> tags;

    @NotBlank
    @ApiModelProperty(required = true)
    private String username;

    public String getFileId() {
        return fileId;
    }

    public String getName() {
        return name;
    }

    public String getProvider() {
        return provider;
    }

    public String getProviderId() {
        return providerId;
    }

    public List<String> getTags() {
        return tags;
    }

    public String getUsername() {
        return username;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()
                + "[fileId: " + fileId
                + " name: " + name
                + " provider: " + provider
                + " providerId: " + providerId
                + " tags: " + tags
                + " username: " + username + "]";
    }
}
