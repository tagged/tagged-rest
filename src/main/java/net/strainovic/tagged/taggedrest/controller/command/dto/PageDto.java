package net.strainovic.tagged.taggedrest.controller.command.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@ApiModel
public class PageDto {

    @NotBlank
    @ApiModelProperty(example = "ASC", allowableValues = "ASC, DESC")
    private String direction = "ASC";

    @Min(0)
    @ApiModelProperty(example = "0", allowableValues = "Number equal or larger than 0")
    private int page = 0;

    @Min(1)
    @ApiModelProperty(example = "25", allowableValues = "Number larger than 0")
    private int size = 25;

    @ApiModelProperty(example = "name")
    private String sortProperty = "name";

    public String getDirection() {
        return direction;
    }

    public int getPage() {
        return page;
    }

    public int getSize() {
        return size;
    }

    public String getSortProperty() {
        return sortProperty;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setSortProperty(String sortProperty) {
        this.sortProperty = sortProperty;
    }
}
