package net.strainovic.tagged.taggedrest.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.strainovic.tagged.taggedrest.controller.command.AddTagCommand;
import net.strainovic.tagged.taggedrest.controller.command.EditTagCommand;
import net.strainovic.tagged.taggedrest.controller.command.SearchCommand;
import net.strainovic.tagged.taggedrest.dao.TaggedDao;
import net.strainovic.tagged.taggedrest.dao.TaggedSearchDao;
import net.strainovic.tagged.taggedrest.model.Tagged;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

@Api(value = "Tags", description = "Managing tags on document", consumes = "application/json", tags = "Tags")
@RestController
public class TagController {

    private static final Log LOGGER = LogFactory.getLog(TagController.class);

    @Autowired
    private TaggedDao taggedDao;

    @Autowired
    private TaggedSearchDao taggedSearchDao;

    @ApiOperation(value = "Edit tags for specific document", httpMethod = "PUT")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Document is saved with new tags"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @PutMapping("/v1/tags")
    public Tagged editTag(@RequestBody @Valid EditTagCommand editTagCommand) {
        Tagged tagged = taggedDao.findOne(editTagCommand.getId());

        Validate.isTrue(tagged != null, String.format("Tag id: %s doesn't exist", editTagCommand.getId()));

        tagged.setTags(editTagCommand.getTags());
        taggedDao.save(tagged);

        return tagged;
    }

    @ApiOperation(value = "Add tags for specific document", httpMethod = "POST")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Document is saved with provided tags"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @PostMapping("/v1/tags")
    public Tagged addTag(@RequestBody @Valid AddTagCommand addTagCommand) {
        String providerId = addTagCommand.getProviderId();
        String provider = addTagCommand.getProvider();

        SearchCommand searchCommand = new SearchCommand();
        searchCommand.setFileIds(Collections.singletonList(addTagCommand.getFileId()));
        searchCommand.setProvider(provider);
        searchCommand.setProviderId(providerId);
        searchCommand.setUsername(addTagCommand.getUsername());

        List<Tagged> existingTags = getTaggedSearchDao().search(searchCommand);

        Validate.isTrue(CollectionUtils.isEmpty(existingTags),
                String.format("Tags for document fileId: %s, provider: %s already exists",
                        addTagCommand.getFileId(), provider));

        Tagged tagged = new Tagged();
        tagged.setFileId(addTagCommand.getFileId());
        tagged.setName(addTagCommand.getName());
        tagged.addTags(addTagCommand.getTags());
        tagged.setProviderId(providerId);

        if (StringUtils.isNotBlank(provider)) {
            tagged.setProvider(provider.toUpperCase());
        }

        tagged.setUsername(addTagCommand.getUsername());

        return getTaggedDao().save(tagged);
    }

    @ApiOperation(value = "Delete all tags and document", httpMethod = "DELETE")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Tags and document are removed"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @DeleteMapping("/v1/tags/{id}")
    public void deleteTag(@PathVariable("id") String id) {
        Long deletedCount = getTaggedDao().deleteById(id);
        LOGGER.debug(String.format("Total %s document deleted for tag: %s", deletedCount, id));
    }


    private TaggedDao getTaggedDao() {
        return taggedDao;
    }

    private TaggedSearchDao getTaggedSearchDao() {
        return taggedSearchDao;
    }

}
