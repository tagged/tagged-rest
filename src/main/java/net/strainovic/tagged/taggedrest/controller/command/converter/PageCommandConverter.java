package net.strainovic.tagged.taggedrest.controller.command.converter;

import net.strainovic.tagged.taggedrest.controller.command.PageCommand;
import net.strainovic.tagged.taggedrest.controller.command.dto.PageCommandDto;
import org.springframework.data.domain.PageRequest;

public final class PageCommandConverter {

    public static PageCommand fromDtoToInternal(PageCommandDto dto) {
        PageCommand internal = new PageCommand();
        internal.setTags(dto.getTags());
        internal.setProvider(dto.getProvider());
        internal.setUsername(dto.getUsername());

        PageRequest pageRequest = PageRequestConverter.fromDtoToInternal(dto.getPageDto());
        internal.setPageRequest(pageRequest);
        return internal;
    }
}
