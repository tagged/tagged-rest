package net.strainovic.tagged.taggedrest.controller.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.strainovic.tagged.taggedrest.model.validation.Tags;

import java.util.Collection;
import java.util.List;

@ApiModel
public class SearchCommand {

    @ApiModelProperty
    private List<String> fileIds;

    @ApiModelProperty
    private String provider;

    @ApiModelProperty
    private String providerId;

    @Tags
    @ApiModelProperty(required = true)
    private Collection<String> tags;

    @ApiModelProperty
    private String username;

    public List<String> getFileIds() {
        return fileIds;
    }

    public String getProvider() {
        return provider;
    }

    public String getProviderId() {
        return providerId;
    }

    public Collection<String> getTags() {
        return tags;
    }

    public String getUsername() {
        return username;
    }

    public void setFileIds(List<String> fileIds) {
        this.fileIds = fileIds;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public void setTags(Collection<String> tags) {
        this.tags = tags;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
