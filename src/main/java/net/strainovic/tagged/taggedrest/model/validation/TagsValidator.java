package net.strainovic.tagged.taggedrest.model.validation;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;

public class TagsValidator implements ConstraintValidator<Tags, Collection<String>> {

    @Override
    public void initialize(Tags constraintAnnotation) {
    }

    @Override
    public boolean isValid(Collection<String> value, ConstraintValidatorContext context) {
        if (CollectionUtils.isEmpty(value)) {
            return false;
        }
        for (String tag : value) {
            if (StringUtils.isBlank(tag)) {
                return false;
            }
            for (char c : tag.toCharArray()) {
                if (!Character.isLetterOrDigit(c)) {
                    return false;
                }
            }
        }
        return true;
    }
}
