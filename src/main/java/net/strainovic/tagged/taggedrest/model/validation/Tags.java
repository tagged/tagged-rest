package net.strainovic.tagged.taggedrest.model.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TagsValidator.class)
@Documented
public @interface Tags {

    String message() default "For tag only leather or digit are allowed";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
