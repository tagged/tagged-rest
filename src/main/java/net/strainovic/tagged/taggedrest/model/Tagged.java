package net.strainovic.tagged.taggedrest.model;

import net.strainovic.tagged.taggedrest.model.validation.Tags;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Document(collection = "tag")
public class Tagged {

    /**
     * ID received from external provider
     */
    @NotBlank
    private String fileId;

    /**
     * Id in database
     */
    @Id
    private String id;

    @NotBlank
    private String name;

    private String provider;

    private String providerId;

    @Tags
    @Indexed
    private List<String> tags;

    @NotBlank
    private String username;

    public Tagged() {
    }

    public Tagged(String fileId, String name, List<String> tags, String username, String provider, String providerId) {
        this.fileId = fileId;
        this.name = name;
        this.tags = tags;
        this.username = username;
        this.provider = provider;
        this.providerId = providerId;
    }

    public void addTags(Collection<String> taggsToAdd) {
        if (tags == null) {
            tags = new ArrayList<>();
        }
        tags.addAll(taggsToAdd);
    }

    public String getFileId() {
        return fileId;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getProvider() {
        return provider;
    }

    public String getProviderId() {
        return providerId;
    }

    public List<String> getTags() {
        return tags;
    }

    public String getUsername() {
        return username;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
