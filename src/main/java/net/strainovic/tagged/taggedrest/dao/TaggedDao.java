package net.strainovic.tagged.taggedrest.dao;

import net.strainovic.tagged.taggedrest.model.Tagged;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TaggedDao extends MongoRepository<Tagged, String> {

    Long deleteById(String id);
}
