package net.strainovic.tagged.taggedrest.dao.impl;

import net.strainovic.tagged.taggedrest.controller.command.PageCommand;
import net.strainovic.tagged.taggedrest.controller.command.SearchCommand;
import net.strainovic.tagged.taggedrest.dao.TaggedSearchDao;
import net.strainovic.tagged.taggedrest.model.Tagged;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public class TaggedSearchDaoImpl implements TaggedSearchDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Page<Tagged> page(PageCommand pageCommand) {
        Query query = createQueryInternal(pageCommand);

        //Total count
        long count = getMongoTemplate().count(query, Tagged.class);

        //Add page to query
        Pageable pageRequest = pageCommand.getPageRequest();
        query.with(pageRequest);
        List<Tagged> list = getMongoTemplate().find(query, Tagged.class);

        return new PageImpl<>(list, pageRequest, count);
    }

    @Override
    public List<Tagged> search(SearchCommand searchCommand) {
        Query query = createQueryInternal(searchCommand);
        return getMongoTemplate().find(query, Tagged.class);
    }

    private Query createQueryInternal(SearchCommand searchCommand) {
        Query query = new Query();

        //Tags
        if (CollectionUtils.isNotEmpty(searchCommand.getTags())) {
            query.addCriteria(Criteria.where("tags").in(searchCommand.getTags()));
        }

        //Provider
        if (StringUtils.isNotBlank(searchCommand.getProvider())) {
            query.addCriteria(Criteria.where("provider").is(searchCommand.getProvider()));
        }

        //ProviderId
        if (StringUtils.isNotBlank(searchCommand.getProviderId())) {
            query.addCriteria(Criteria.where("providerId").is(searchCommand.getProviderId()));
        }

        //Username
        if (StringUtils.isNotBlank(searchCommand.getUsername())) {
            query.addCriteria(Criteria.where("username").is(searchCommand.getUsername()));
        }

        //External ids
        if(CollectionUtils.isNotEmpty(searchCommand.getFileIds())) {
            query.addCriteria(Criteria.where("fileId").in(searchCommand.getFileIds()));
        }

        return query;
    }

    private MongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }

}
