package net.strainovic.tagged.taggedrest.dao;

import net.strainovic.tagged.taggedrest.controller.command.PageCommand;
import net.strainovic.tagged.taggedrest.controller.command.SearchCommand;
import net.strainovic.tagged.taggedrest.model.Tagged;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaggedSearchDao {

    Page<Tagged> page(PageCommand pageCommand);

    List<Tagged> search(SearchCommand searchCommand);

}
