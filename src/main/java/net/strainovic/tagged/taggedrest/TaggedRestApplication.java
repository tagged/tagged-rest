package net.strainovic.tagged.taggedrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaggedRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaggedRestApplication.class, args);
	}
}
