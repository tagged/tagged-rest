package net.strainovic.tagged.taggedrest.configuration;

import net.strainovic.tagged.taggedrest.dao.TaggedSearchDao;
import net.strainovic.tagged.taggedrest.dao.impl.TaggedSearchDaoImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomDaoConfiguration {

    @Bean
    public TaggedSearchDao getTaggedSearchDao() {
        return new TaggedSearchDaoImpl();
    }
}
